import axios from 'axios'

export const getReposPromise = (url) => {
  return axios.get(url)
}

export const getByOrganization = (org, fromLocal = false) => {
  return {
    type: 'REPOSITORIES',
    payload: getReposPromise(fromLocal ? '/assets/fakedata/nodejsRepos.json' : `https://api.github.com/orgs/${org}/repos`),
    meta: {
      org
    }
  }
}
