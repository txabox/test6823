import { toogleVisibility } from './IssuesActionCreators'

describe('IssuesActionCreators', () => {
  it('+++ toogleVisibility method', () => {
    const repoName = 'http-parser'
    return expect(toogleVisibility(repoName)).toEqual({
      type: 'ISSUES_TOOGLE_VISIBILITY',
      payload: {
        repoName
      }
    })
  })
})
