import axios from 'axios'

export const getByRepoName = (repoName, fromLocal = false) => ({
  type: 'ISSUES',
  payload: axios.get(fromLocal ? '/assets/fakedata/repoIssues.json' : `https://api.github.com/repos/nodejs/${repoName}/issues?state=open`),
  meta: {
    repoName
  }
})

export const toogleVisibility = (repoName) => ({
  type: 'ISSUES_TOOGLE_VISIBILITY',
  payload: {
    repoName
  }
})
