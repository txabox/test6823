import { getReposPromise } from './RepositoriesActionCreators'
jest.mock('../../__mocks__/axios')

describe('RepositoriesActionCreators', () => {
  it('+++ getReposPromise method with existing organization', () => {
    const org = 'nodejs'
    return getReposPromise(`https://api.github.com/orgs/${org}/repos`)
      .then(data => expect(data).toBe(true))
  })

  it('+++ getReposPromise method with NON existing organization', () => {
    const org = 'nodejs2'
    return getReposPromise(`https://api.github.com/orgs/${org}/repos`)
      .catch(error => {
        return expect(error).toEqual(new Error('Organization not found.'))
      })
  })
})
