import {
  createStore,
  applyMiddleware,
  combineReducers
} from 'redux'

import promiseMiddleware from 'redux-promise-middleware'

// Logger with default options
// import logger from 'redux-logger'

import repositories from './reducers/repositories/repositoriesReducer'
import issues from './reducers/issues/issuesReducer'

const reducers = combineReducers({
  repositories,
  issues
})

export default createStore(
  reducers,
  applyMiddleware(
    // logger,
    promiseMiddleware()
  ))
