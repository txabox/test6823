import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as RepositoriesActionCreators from 'Actions/RepositoriesActionCreators'

import Repository from './components/Repository/Repository'

const StyledRepositories = styled.ul`
`

export class Repositories extends React.Component {
  componentDidMount() {
    this.props.actions.repositories.getByOrganization(this.props.organization)
  }

  render() {
    return <StyledRepositories>
      {
        !this.props.isFulfilled
        ? <li>Loading...</li>
        : this.props.array.map((repo) => {
          return <Repository key={repo.id} repo={repo} />
        })

      }
    </StyledRepositories>
  }
}

Repositories.propTypes = {
  organization: PropTypes.string.isRequired,
  array: PropTypes.array.isRequired,
  isFulfilled: PropTypes.bool.isRequired,
  actions: PropTypes.object.isRequired
}

export const mapStateToProps = (state) => {
  return {
    array: state.repositories.array,
    isFulfilled: state.repositories.isFulfilled
  }
}

export const mapDispatchToProps = (dispatch) => {
  return {
    actions: {
      repositories: bindActionCreators(RepositoriesActionCreators, dispatch)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Repositories)
