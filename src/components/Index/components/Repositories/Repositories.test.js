import React from 'react'
import { configure, shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import Adapter from 'enzyme-adapter-react-16'
import { Repositories, mapStateToProps, mapDispatchToProps } from './Repositories'

configure({ adapter: new Adapter() })

describe('Repositories Component', () => {
  let wrapper
  let tree

  const basicProps = {
    organization: 'nodejs',
    array: [],
    isFulfilled: false,
    actions: {
      repositories: {
        getByOrganization: () => {}
      }
    }
  }

  beforeEach(() => {
    wrapper = shallow(<Repositories {...basicProps} />)
    tree = toJson(wrapper)
  })

  it('+++ render the DUMB component', () => {
    expect(wrapper.length).toBe(1)
  })

  it('+++ renders props correctly', () => {
    expect(wrapper.instance().props.organization).toBe('nodejs')
  })

  it('+++ check propper number of repositories when isFulfilled equals to true', () => {
    const array = [{id: 1}, {id: 2}, {id: 3}]
    wrapper = shallow(<Repositories {...basicProps} isFulfilled={true} array={array} />)
    expect(wrapper.children().length).toBe(array.length)
  })

  it('+++ checks mapStateToProps 1', () => {
    const initialState = {
      repositories: {
        array: [],
        isFulfilled: true
      }
    }
    expect(mapStateToProps(initialState).isFulfilled).toEqual(true)
  })

  it('+++ checks mapStateToProps 2', () => {
    const initialState = {
      repositories: {
        array: [{
          id: 1
        }],
        isFulfilled: true
      }
    }
    expect(mapStateToProps(initialState).array.length).toEqual(1)
  })

  it('+++ checks mapDispatchToProps', () => {
    const dispatch = jest.fn()
    mapDispatchToProps(dispatch).actions.repositories.getByOrganization(basicProps.organization)
    expect(dispatch.mock.calls[0][0]).toEqual({
      type: 'REPOSITORIES',
      payload: Promise.resolve(),
      meta: {
        org: basicProps.organization
      }
    })
  })

  it('+++ compare snapshots', () => {
    expect(tree).toMatchSnapshot()
  })
})
