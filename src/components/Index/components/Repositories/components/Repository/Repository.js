import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as IssuesActionCreators from 'Actions/IssuesActionCreators'

import Issues from './components/Issues/Issues'

const StyledRepository = styled.li`
  >span{
    cursor: pointer;
  }
`

export class Repository extends React.Component {
  handleClick = () => {
    const {actions, repo, issues} = this.props
    issues
      ? actions.issues.toogleVisibility(repo.name)
      : actions.issues.getByRepoName(repo.name)
  }

  render() {
    const {repo, issues} = this.props
    return <StyledRepository>
      <span onClick={this.handleClick}>
        {repo.name}
      </span>
      { issues
      ? <Issues issues={issues} />
      : null
      }
    </StyledRepository>
  }
}

Repository.propTypes = {
  repo: PropTypes.object.isRequired,
  issues: PropTypes.object,
  actions: PropTypes.object.isRequired
}

export const mapStateToProps = (state, ownProps) => {
  const {name} = ownProps.repo
  return {
    issues: state.issues.map[name]
  }
}

export const mapDispatchToProps = (dispatch) => {
  return {
    actions: {
      issues: bindActionCreators(IssuesActionCreators, dispatch)}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Repository)
