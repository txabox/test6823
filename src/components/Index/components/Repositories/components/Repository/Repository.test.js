import React from 'react'
import { configure, shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import Adapter from 'enzyme-adapter-react-16'
import { Repository, mapStateToProps, mapDispatchToProps } from './Repository'

configure({ adapter: new Adapter() })

describe('Repository Component', () => {
  let wrapper
  let tree

  const basicProps = {
    repo: {
      name: 'http-parser'
    },
    issues: null,
    actions: {
      issues: {
        toogleVisibility: () => {},
        getByRepoName: () => {}
      }
    }
  }

  beforeEach(() => {
    wrapper = shallow(<Repository {...basicProps} />)
    tree = toJson(wrapper)
  })

  it('+++ render the DUMB component', () => {
    expect(wrapper.length).toBe(1)
  })

  it('+++ renders props correctly', () => {
    expect(wrapper.instance().props.repo.name).toBe(basicProps.repo.name)
  })

  it('+++ renders repo name correctly', () => {
    expect(wrapper.find('span').text()).toBe(basicProps.repo.name)
  })

  it('+++ renders issues ul correctly', () => {
    const issues = {
      areVisible: true,
      array: []
    }
    wrapper = shallow(<Repository {...basicProps} issues={issues} />)
    expect(wrapper.children().at(1).length).toEqual(1)
  })

  it('+++ checks mapStateToProps', () => {
    const initialState = {
      issues: {
        map: {
          [basicProps.repo.name]: {
            id: 1
          }
        }
      }
    }
    const ownProps = {
      repo: {
        name: basicProps.repo.name
      }
    }
    expect(mapStateToProps(initialState, ownProps).issues.id).toEqual(1)
  })

  it('+++ checks mapDispatchToProps', () => {
    const dispatch = jest.fn()
    mapDispatchToProps(dispatch).actions.issues.toogleVisibility(basicProps.repo.name)
    expect(dispatch.mock.calls[0][0]).toEqual({
      payload: {
        repoName: basicProps.repo.name
      },
      type: 'ISSUES_TOOGLE_VISIBILITY'
    })
  })

  it('+++ compare snapshots', () => {
    expect(tree).toMatchSnapshot()
  })
})
