import React from 'react'
import { configure, shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import Adapter from 'enzyme-adapter-react-16'
import Issues from './Issues'

configure({ adapter: new Adapter() })

describe('Issues Component', () => {
  let wrapper
  let tree

  const basicProps = {
    issues: {
      isPending: true,
      areVisible: false,
      array: []
    }
  }

  beforeEach(() => {
    wrapper = shallow(<Issues {...basicProps} />)
    tree = toJson(wrapper)
  })

  it('+++ render the DUMB component', () => {
    expect(wrapper.length).toBe(1)
  })

  it('+++ render the DUMB component once issues are fullfilled', () => {
    const PropsOnIssuesFullfilled = {
      issues: {
        isPending: false,
        areVisible: true,
        array: [{
          id: 1,
          title: 'Issue Title #1'
        },
        {
          id: 2,
          title: 'Issue Title #2'
        },
        {
          id: 3,
          title: 'Issue Title #3'
        }]
      }
    }
    wrapper = shallow(<Issues {...PropsOnIssuesFullfilled} />)
    expect(wrapper.children().length).toBe(PropsOnIssuesFullfilled.issues.array.length)
  })

  it('+++ compare snapshots', () => {
    expect(tree).toMatchSnapshot()
  })
})
