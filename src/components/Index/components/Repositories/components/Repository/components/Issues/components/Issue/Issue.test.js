import React from 'react'
import { configure, shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import Adapter from 'enzyme-adapter-react-16'
import Issue from './Issue'

configure({ adapter: new Adapter() })

describe('Issue Component', () => {
  let wrapper
  let tree

  const basicProps = {
    issue: {
      id: 1,
      title: 'Issue Title #1'
    }
  }

  beforeEach(() => {
    wrapper = shallow(<Issue {...basicProps} />)
    tree = toJson(wrapper)
  })

  it('+++ render the DUMB component', () => {
    expect(wrapper.length).toBe(1)
  })

  it('+++ compare snapshots', () => {
    expect(tree).toMatchSnapshot()
  })
})
