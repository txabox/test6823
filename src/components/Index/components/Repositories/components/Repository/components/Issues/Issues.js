import React from 'react'
import PropTypes from 'prop-types'

import Issue from './components/Issue/Issue'

const Issues = ({issues}) => <ul>
  {
    issues.isPending
    ? <li>Loading...</li>
    : issues.areVisible
    ? issues.array.map(issue => <Issue key={issue.id} issue={issue} />)
    : null
  }
</ul>

Issues.propTypes = {
  issues: PropTypes.object.isRequired
}

export default Issues
