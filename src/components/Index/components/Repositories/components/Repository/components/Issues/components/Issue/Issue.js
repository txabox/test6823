import React from 'react'
import PropTypes from 'prop-types'

const Issue = ({issue}) => <li>{issue.title}</li>

Issue.propTypes = {
  issue: PropTypes.object.isRequired
}

export default Issue
