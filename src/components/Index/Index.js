import React from 'react'
import styled from 'styled-components'

import Repositories from './components/Repositories/Repositories'

const StyledIndex = styled.div`
  height: 100%;
`

const ORG = 'nodejs'

const Index = () => {
  return <StyledIndex>
    <h1>{ORG} repositories:</h1>
    <Repositories organization={ORG} />
  </StyledIndex>
}

export default Index
