import issuesReducer, { TYPE, defaultValues, defaultRepositoryIssues } from './issuesReducer'

describe('RepositoriesReducer', () => {
  let state
  it('+++ has a default state', () => {
    const action = {
      type: 'unexpected'
    }
    expect(issuesReducer(state, action)).toEqual(defaultValues)
  })

  it(`+++ can handle ${TYPE}_PENDING`, () => {
    const action = {
      type: `${TYPE}_PENDING`,
      payload: {},
      meta: {
        repoName: 'http-parser'
      }
    }
    const map = {
      ...defaultValues.map,
      [action.meta.repoName]: {
        ...defaultRepositoryIssues,
        isPending: true
      }
    }
    const properState = {
      ...defaultValues,
      map
    }
    expect(issuesReducer(state, action)).toEqual(properState)
  })

  it(`+++ can handle ${TYPE}_FULFILLED`, () => {
    const action = {
      type: `${TYPE}_FULFILLED`,
      payload: {
        data: [1, 2, 3]
      },
      meta: {
        repoName: 'http-parser'
      }
    }
    const map = {
      ...defaultValues.map,
      [action.meta.repoName]: {
        ...defaultRepositoryIssues,
        isPending: false,
        isFulfilled: true,
        isRejected: false,
        areVisible: true,
        data: action.payload,
        array: action.payload.data,
        error: null
      }
    }
    const properState = {
      ...defaultValues,
      map
    }
    expect(issuesReducer(state, action)).toEqual(properState)
  })

  it(`+++ can handle ${TYPE}_REJECTED`, () => {
    const action = {
      type: `${TYPE}_REJECTED`,
      payload: 'Error',
      meta: {
        repoName: 'http-parser',
        DONT_PRINT_ERRORS: true
      }
    }
    const map = {
      ...defaultValues.map,
      [action.meta.repoName]: {
        ...defaultRepositoryIssues,
        isRejected: true,
        error: action.payload
      }
    }
    const properState = {
      ...defaultValues,
      map
    }
    expect(issuesReducer(state, action)).toEqual(properState)
  })

  it(`+++ can handle ${TYPE}_TOOGLE_VISIBILITY`, () => {
    const repoName = 'http-parser'
    const OldState = {
      map: {
        [repoName]: {
          ...defaultRepositoryIssues
        }
      }
    }
    const action = {
      type: `${TYPE}_TOOGLE_VISIBILITY`,
      payload: {
        repoName
      }
    }

    const newState = {
      map: {
        [repoName]: {
          ...defaultRepositoryIssues,
          areVisible: !OldState.map[repoName].areVisible
        }
      }
    }
    expect(issuesReducer(OldState, action)).toEqual(newState)
  })
})
