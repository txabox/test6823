export const TYPE = 'ISSUES'

export const defaultValues = {
  map: {}
}

export const defaultRepositoryIssues = {
  isPending: false,
  isFulfilled: false,
  isRejected: false,
  areVisible: false,
  data: null,
  array: [],
  error: null
}

// Handle the action
export default (state = defaultValues, action) => {
  switch (action.type) {
    case `${TYPE}_PENDING`:
    {
      const {repoName} = action.meta
      return {
        map: {
          ...state.map,
          [repoName]: {
            ...defaultRepositoryIssues,
            isPending: true
          }
        }

      }
    }

    case `${TYPE}_FULFILLED`:
    {
      const {repoName} = action.meta
      return {
        map: {
          ...state.map,
          [repoName]: {
            isPending: false,
            isFulfilled: true,
            isRejected: false,
            areVisible: true,
            data: action.payload,
            array: action.payload.data,
            error: null
          }
        }

      }
    }

    case `${TYPE}_REJECTED`:
    {
      if (!action.meta || !action.meta.DONT_PRINT_ERRORS) {
        console.error(`Error retrieving ${TYPE}: `, action.payload)
      }
      const {repoName} = action.meta
      return {
        map: {
          ...state.map,
          [repoName]: {
            ...defaultRepositoryIssues,
            isRejected: true,
            error: action.payload
          }
        }

      }
    }

    case `${TYPE}_TOOGLE_VISIBILITY`:
    {
      const {repoName} = action.payload
      return {
        map: {
          ...state.map,
          [repoName]: {
            ...state.map[repoName],
            areVisible: !state.map[repoName].areVisible
          }
        }
      }
    }

    default:
      return state
  }
}
