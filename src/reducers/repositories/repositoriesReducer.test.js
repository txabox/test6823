import repositoriesReducer, { TYPE, defaultValues } from './repositoriesReducer'

describe('RepositoriesReducer', () => {
  const state = undefined
  it('+++ has a default state', () => {
    const action = {
      type: 'unexpected'
    }
    expect(repositoriesReducer(state, action)).toEqual(defaultValues)
  })

  it(`+++ can handle ${TYPE}_PENDING`, () => {
    const action = {
      type: `${TYPE}_PENDING`,
      payload: {},
      meta: {
        org: 'nodejs'
      }
    }
    const properState = {
      ...defaultValues,
      name: action.meta.org
    }
    expect(repositoriesReducer(state, action)).toEqual(properState)
  })

  it(`+++ can handle ${TYPE}_FULFILLED`, () => {
    const action = {
      type: `${TYPE}_FULFILLED`,
      payload: {
        data: [1, 2, 3]
      }
    }
    const properState = {
      ...defaultValues,
      isPending: false,
      isFulfilled: true,
      data: action.payload,
      array: action.payload.data
    }
    expect(repositoriesReducer(state, action)).toEqual(properState)
  })

  it(`+++ can handle ${TYPE}_REJECTED`, () => {
    const action = {
      type: `${TYPE}_REJECTED`,
      payload: 'Error',
      meta: {
        DONT_PRINT_ERRORS: true
      }
    }
    const properState = {
      ...defaultValues,
      isRejected: true,
      error: action.payload
    }
    expect(repositoriesReducer(state, action)).toEqual(properState)
  })
})
