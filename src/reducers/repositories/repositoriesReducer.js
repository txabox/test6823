export const TYPE = 'REPOSITORIES'

export const defaultValues = {
  name: null,
  data: null,
  array: [],
  error: null,
  isPending: true, // default because it is loading from the very begining
  isFulfilled: false,
  isRejected: false
}

// Handle the action
export default (state = defaultValues, action) => {
  switch (action.type) {
    case `${TYPE}_PENDING`:
    {
      return {
        ...defaultValues,
        name: action.meta.org
      }
    }

    case `${TYPE}_FULFILLED`:
    {
      return {
        ...defaultValues,
        isPending: false,
        isFulfilled: true,
        data: action.payload,
        array: action.payload.data
      }
    }

    case `${TYPE}_REJECTED`:
    {
      if (!action.meta || !action.meta.DONT_PRINT_ERRORS) {
        console.error(`Error retrieving ${TYPE}: `, action.payload)
      }
      return {
        ...defaultValues,
        isRejected: true,
        error: action.payload
      }
    }

    default:
      return state
  }
}
