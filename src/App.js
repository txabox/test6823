import React from 'react'
import ReactDOM from 'react-dom'
import Router from './Router'

import {Provider} from 'react-redux'
import store from './store'

import './App.scss'

if (process.env.NODE_ENV !== 'production') {
  console.warn('Looks like we are in development mode!')
}

ReactDOM.render(
  <Provider store={store}>
    <div className="container full-height">
      <Router />
    </div>
  </Provider>,
  document.getElementById('test6823App')
)
