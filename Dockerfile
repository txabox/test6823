FROM ubuntu:latest
MAINTAINER Txabi Lopez <lauteilatu@hotmail.com>

RUN apt-get update && apt-get install -y \
    curl \
    git


RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.6/install.sh | bash

ENV NVM_DIR=/root/.nvm
ENV SHIPPABLE_NODE_VERSION=v9.3.0

# From here we load our application's code in, therefore the previous docker
# "layer" thats been cached will be used if possible
WORKDIR /opt/app
RUN git clone https://bitbucket.org/txabox/test6823.git
RUN cp -r test6823/. . && rm -r test6823

ENV PATH="./node_modules/.bin:${PATH}"
ENV NODE_ENV production


RUN . $NVM_DIR/nvm.sh && \
  nvm install $SHIPPABLE_NODE_VERSION && \
  nvm alias default $SHIPPABLE_NODE_VERSION && \
  nvm use default && \
  npm install -g pm2 && \
  npm install && \
  npm run start:front:prod && \
  npm run start:back:prod

ENV PATH="/root/.nvm/versions/node/v9.3.0/bin/:${PATH}"


EXPOSE 8086
CMD ["pm2", "start", "/opt/app/server/server.js", "--interpreter", "babel-node", "--no-daemon"]
