const validOrganizations = [
  'nodejs'
]

export default {
  get: (url) => new Promise((resolve, reject) => {
    const getOrgNameFromURL = (url) => {
      let org = url.substr('https://api.github.com/orgs/'.length)
      return org.substr(0, org.lastIndexOf('/'))
    }

    const org = getOrgNameFromURL(url)
    process.nextTick(
      () =>
        validOrganizations.includes(org)
          ? resolve(true)
          : reject(new Error('Organization not found.'))
    )
  })

}
