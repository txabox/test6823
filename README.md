# README #

### test6823 React Front-End Interview Test

# Brief

The goal of this task is to create an application consisting of:

## Client-side

A client-side single-page application which must connect to the github API endpoint (see below) to:

- List the repositories from the `nodejs` organisation, showing the name of each

- When clicking on a repository, expand and make a request to get the open issues from that repository

- Once the issues from the repository are retrieved, show them under the repo as nested items, by title

- Have an intermediate loading state (show "loading..." as the only item during this process)

- Handle errors while requesting the API by logging to the console

- A simple ul-li structure is fine, no styling necessary

- Only request issues once per repository

Github API:

Get repositories by org: `https://api.github.com/orgs/nodejs/repos`

Get open issues by repository: `https://api.github.com/repos/nodejs/reponame/issues?state=open`




## Considerations

- Write tests, but no need for 100% coverage, just the main cases

- The code should be easy to read.

- Feel free to use babel along with any ES features that are stage-3 and up

- Feel free to use any tooling chain you're comfortable with

- Do not use callbacks for async calls - use either promises or async/await

- Feel free to use any UI library such as React, Vue, etc, and any state management library such as Redux, MobX, etc. Also feel free not to use any.

- Provide instructions to install, test and run the app

Please do get in touch at any time if you have any questions. We appreciate you using your personal time to complete this task, so let us know if we can help.





## Live Server
Yon see the app up and running live from here: http://ec2-54-154-35-216.eu-west-1.compute.amazonaws.com:8086/


## Testing
For testing run:
```
npm run test
```

or
```
npm run test:watch
```


## How to install the App on your local laptop

### With Docker:
1. Install the docker client for your machine if not installed previously. Instructions: https://docs.docker.com/install/
2. Copy the Dockerfile (https://bitbucket.org/txabox/test6823/src) in your desired folder.
3. Open a terminal on that folder and build the test6823 image running this code (this might take a while):
```
docker build -t test6823_image .
```
4. Run a container out from the previous image running this code:
```
docker run -p 8086:8086 --rm -i --name test6823_container test6823_image
```
5. You can access the web going to http://localhost:8086/ on your browser
6. To stop the container running you have to open a new terminal and run this code:
```
docker stop test6823_container
```
7. And finally run this code to remove all the used images:
```
docker rmi test6823_image ubuntu
```

### Without Docker:
1. Install nvm if is not installed on your computer. Instructions: https://github.com/creationix/nvm
2. Set the node version to v9.3.0 in nvm. Instructions: https://www.sitepoint.com/quick-tip-multiple-versions-node-nvm/
3. Install pm2 globally if is not installed with:
```
npm install pm2 -g
```
4. Install git if not installed. Instructions here: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
5. Clone the repository to the desired folder: https://bitbucket.org/txabox/test6823
6. Install all the dependencies with:
```
npm install
```
7. Package all the files with webpack like this (choose whether you want the App to be run in 'dev' or 'prod' environment):
```
npm run start:front:[dev|prod]
```
8. Run the express server (running in port 8086) (choose whether you want the App to be run in 'dev' or 'prod' environment):
```
npm run start:back:[dev|prod]
```
9. You can access the web going to http://localhost:8086/ on your browser
