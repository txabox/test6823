import config from 'config'
import path from 'path'
import express from 'express'
import bodyParser from 'body-parser'
import morgan from 'morgan'
import session from 'express-session'
import webRouter from './routes/web/webRouter'

const app = express()

/** *********************** EXPRESS SETTINGS: *************************/

app.set('views', path.join(__dirname, '/../public/views'))

// use twig as the template engine
// Set render engine
app.set('view engine', 'twig')
// This section is optional and used to configure twig.
app.set('twig options', {
  strict_variables: false
})

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))
app.use(morgan('dev'))

// make all assets (.js, .css, images, etc) in public accesible
app.use(express.static(path.join(__dirname, '/../public')))

app.use(session({
  secret: '?ZXZ2"s9y6^y-8Pa',
  saveUninitialized: true,
  resave: true
}))

// To make this server CORS-ENABLE
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

/** *********************** EXPRESS ROUTERS: *************************/
app.use('/*', webRouter)

/** *********************** EXPRESS INIT: *************************/
app.listen(config.server.port, () => {
  console.warn('Express listening on port ', config.server.port)
})
